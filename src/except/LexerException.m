/*
 * LexerException.m
 * Implements an exception that is thrown when the lexer finds an error
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "LexerException.h"

//class implementation
@implementation LexerException

//init method - initializes a LexerException instance
- (id) initWithLine: (int) line andColumn: (int) column
	andCharacter: (char) badChar {
	//call the superclass init method
	self = [super initWithName: @"LexerException"
			    reason:
[NSString stringWithFormat: @"(%d:%d) error: unknown character %c",
	line, column, badChar]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates a LexerException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased 
//LexerException instance
+ (LexerException*) exceptionWithLine: (int) line andColumn: (int) column
	andCharacter: (char) badChar {
	//generate an autoreleased instance
	return [[[LexerException alloc] initWithLine: line
			andColumn: column andCharacter: badChar]
			autorelease];
}

@end //end of implementation

/*
 * LexerException.h
 * Declares an exception that is thrown when the lexer finds an error
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface LexerException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes a LexerException instance
- (id) initWithLine: (int) line andColumn: (int) column 
	andCharacter: (char) badChar;

//deallocates a LexerException instance
- (void) dealloc;

//generates an autoreleased LexerException instance
+ (LexerException*) exceptionWithLine: (int) line andColumn: (int) column
	andCharacter: (char) badChar;

@end //end of header

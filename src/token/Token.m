/*
 * Token.m
 * Implements a class that represents a lexed symbol
 * Created on 9/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 */

//import the class header
#import "Token.h"

//import C standard I/O
#include <stdio.h>

//class implementation
@implementation Token

//property synthesis
@synthesize type = _type;
@synthesize value = _value;

//first init method - initializes a Token with no value
- (id) initWithType: (NSString*) newType {
	//call the other init method
	return [self initWithType: newType andValue: nil];
}

//second init method - initializes a Token with a type and value
- (id) initWithType: (NSString*) newType andValue: (NSString*) newValue {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_type = newType;
		[_type retain];
		_value = newValue;
		if(_value != nil) {
			[_value retain];
		}
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a Token instance
- (void) dealloc {
	//release the type field
	[_type release]; 

	//release the value field
	if(_value != nil) {
		[_value release];
	}

	//and call the superclass dealloc method
	[super dealloc];
}

//print method - prints a Token's data to the console
- (void) print {
	if(_value != nil) {
		printf("Token %p with type %s and value %s\n",
				self, [_type UTF8String],
				[_value UTF8String]);
	} else {
		printf("Token %p with type %s and no value\n",
				self, [_type UTF8String]);
	}
}

@end //end of implementation

/*
 * Token.h
 * Declares a class that represents a lexed symbol
 * Created on 9/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface Token : NSObject {
	//fields
	NSString* _type; //the type of the Token
	NSString* _value; //the value of the Token
}

//property declarations
@property (readonly) NSString* type;
@property (readonly) NSString* value;

//method declarations

//initializes a Token instance with no value
- (id) initWithType: (NSString*) newType;

//initializes a Token instance with a value and a type
- (id) initWithType: (NSString*) newType andValue: (NSString*) newValue;

//deallocates a Token instance
- (void) dealloc;

//prints out a Token instance
- (void) print;

@end //end of header

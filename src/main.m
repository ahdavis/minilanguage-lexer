/*
 * main.m
 * Main code file for the lexer
 * Created on 9/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 */

//imports
#import <stdio.h>
#import <Foundation/Foundation.h>
#import "token/Token.h"
#import "core/Lexer.h"

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//create an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	//make sure that an argument was provided
	if(argc < 2) {
		printf("Usage: %s <filename>\n", argv[0]);
		return EXIT_FAILURE;
	}

	//read in the source file
	NSString* source = [[NSString alloc] initWithContentsOfFile:
				[NSString stringWithFormat: @"%s",
					argv[1]]];

	//create a Lexer instance
	Lexer* lexer = [[Lexer alloc] initWithText: source];

	//get the first token
	Token* curToken = [lexer nextToken];

	//loop through the source text and print each token
	do {
		printf("Line %d, column %d: ", lexer.currentLine,
				lexer.currentColumn);
		[curToken print];
		curToken = [lexer nextToken];
	} while(![[curToken type] isEqualToString: @"EOF"]);

	//release the source string
	[source release];

	//release the lexer
	[lexer release];

	//drain the pool
	[pool drain];

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program

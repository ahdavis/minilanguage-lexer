/*
 * Lexer.h
 * Declares a class that lexes text
 * Created on 10/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../token/Token.h"

//class declaration
@interface Lexer : NSObject {
	//fields
	NSString* _text; //the text being lexed
	int _currentLine; //the current line in the text
	int _currentColumn; //the current column in the text
	int _pos; //the current position in the text
	char _curChar; //the current character being lexed
}

//property declarations
@property (readonly) int currentLine;
@property (readonly) int currentColumn;

//method declarations

//initializes a Lexer instance
- (id) initWithText: (NSString*) newText;

//deallocates a Lexer instance
- (void) dealloc;

//returns an autoreleased Token instance referencing the last lexeme
- (Token*) nextToken;

@end //end of header

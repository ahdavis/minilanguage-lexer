/*
 * Lexer.m
 * Implements a class that lexes text
 * Created on 10/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "Lexer.h"

//import the LexerException class header
#import "../except/LexerException.h"

//private method declarations
@interface Lexer ()

//advances the lexer to the next character
- (void) advance;

//skips whitespace in the text
- (void) skipWhitespace;

//returns an integer consumed from the input
- (NSString*) integer;

//returns a boolean literal or identifier consumed from the input
- (NSString*) boolOrID;

//returns the reserved words in the language
- (NSArray*) reservedWords;

//returns the single-character symbols in the language
- (NSArray*) reservedChars;

@end //end of private methods

//class implementation
@implementation Lexer

//property synthesis
@synthesize currentLine = _currentLine;
@synthesize currentColumn = _currentColumn;

//init method - initializes a Lexer instance
- (id) initWithText: (NSString*) newText {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_text = newText;
		[_text retain];
		_currentLine = 1;
		_currentColumn = 1;
		_pos = 0;
		_curChar = [_text characterAtIndex: _pos];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a Lexer instance
- (void) dealloc {
	[_text release]; //release the text field
	[super dealloc]; //and call the superclass dealloc method
}

//nextToken method - returns the next token consumed from the input
- (Token*) nextToken {
	//loop through the text
	while(_curChar != '\0') {
		//handle spaces
		if((_curChar == ' ') || (_curChar == '\t')) {
			[self skipWhitespace];
			continue;
		}

		//handle comments and division
		if(_curChar == '/') {
			[self advance];
			if(_curChar == '/') {
				while(_curChar != '\n' && 
					_curChar != '\r') {
					[self advance];
				}
				[self advance];
				continue;
			} else {
				return [[[Token alloc] initWithType: @"/"]
						autorelease];
			}
		}

		//handle integers
		if(isdigit(_curChar)) {
			return [[[Token alloc] initWithType: @"NUM"
					andValue: [self integer]]
					autorelease];
		}

		//handle other identifiers
		if(isalpha(_curChar)) {
			//get the identifier from the text
			NSString* sym = [self boolOrID];

			//handle keywords
			for(NSString* word in [self reservedWords]) {
				if([sym isEqualToString: word]) {
					return [[[Token alloc]
						initWithType: sym]
						autorelease];
				}
			}

			//if control reaches here, then
			//no keywords matched the symbol
			//so return an identifier token
			return [[[Token alloc] initWithType: @"ID"
					andValue: sym] autorelease];
		}

		//handle the assignment operator
		if(_curChar == ':') {
			[self advance];
			if(_curChar != '=') {
				@throw [LexerException 
				exceptionWithLine: _currentLine
					andColumn: _currentColumn
				     andCharacter: _curChar];
			} else {
				[self advance];
				return [[[Token alloc] initWithType: @":="
						andValue: @":="]
						autorelease];
			}
		}

		//handle end-of-line characters
		if(_curChar == '\n' || _curChar == '\r') {
			[self advance];
			_currentLine++;
			_currentColumn = 1;
			continue;
		}

		//handle single-character tokens
		for(NSString* word in [self reservedChars]) {
			NSString* sym = [[NSString alloc] 
				initWithFormat: @"%c", _curChar];
			if([sym isEqualToString: word]) {
				[self advance];
				Token* t = [[[Token alloc] 
					initWithType: sym]
					autorelease];
				[sym release];
				return t;
			}
			[sym release];
		}


		//if control reaches here, then no match was found
		//so throw an exception
		@throw [LexerException exceptionWithLine: _currentLine
				andColumn: _currentColumn
			     andCharacter: _curChar];
	}

	//and return an EOF token
	return [[[Token alloc] initWithType: @"EOF"] autorelease];
}

//private advance method - advances the lexer
- (void) advance {
	//increment the position counter
	_pos++; 
	_currentColumn++;

	//and handle EOF
	if(_pos > ([_text length] - 1)) {
		_curChar = '\0';
	} else {
		_curChar = [_text characterAtIndex: _pos];
	}
}

//private skipWhitespace method - skips whitespace in the input
- (void) skipWhitespace {
	//loop through the whitespace
	while((_curChar != '\0') && ((_curChar == ' ') ||
					(_curChar == '\t'))) {
		[self advance];
	}
}

//private integer method - returns an integer consumed from the input
- (NSString*) integer {
	//declare the return object
	NSString* ret = @""; 

	//loop and get the integer string
	while(((_curChar != '\0') && (_curChar != '\n')) 
			&& isdigit(_curChar)) {
		ret = [ret stringByAppendingFormat: @"%c", _curChar];
		[self advance];
	}

	//and return the integer string
	return ret;
}

//private boolOrID method - returns a boolean literal or identifier
//consumed from the input
- (NSString*) boolOrID {
	//declare the return object
	NSString* ret = @"";

	//loop and get the data
	while(((_curChar != '\0') && (_curChar != '\n'))
			&& (isalnum(_curChar) ||
					(_curChar == '_'))) {
		ret = [ret stringByAppendingFormat: @"%c", _curChar];
		[self advance];
	}

	//and return the data string
	return ret;
}

//private reservedWords method - returns the reserved words
//for the language
- (NSArray*) reservedWords {
	//return an array of the reserved words
	return [NSArray arrayWithObjects:
		@"begin",
		@"end",
		@"bool",
		@"int",
		@"if",
		@"then",
		@"else",
		@"fi",
		@"while",
		@"do",
		@"od",
		@"print",
		@"or",
		@"and",
		@"not",
		@"false",
		@"true", nil];
}

//private reservedChars method - returns the reserved characters
//in the language
- (NSArray*) reservedChars {
	//return the reserved characters
	return [NSArray arrayWithObjects:
		@";",
		@"=",
		@"<",
		@"+",
		@"-",
		@"*",
		@"(",
		@")", nil];
}

@end //end of implementation

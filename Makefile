# Makefile for minilanguage-lexer
# Compiles the code for the lexer
# Created on 9/27/18
# Created by Andrew Davis
#
# Copyright (C) 2018 Andrew Davis

# define the compiler
CC=gcc

# define the compiler flags
CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall

# define state-specific compiler flags
debug: CFLAGS += -g

# define linker flags
LDFLAGS=-lgnustep-base -lobjc

# retrieve source code for the lexer
MAIN=$(shell ls src/*.m)
TOK=$(shell ls src/token/*.m)
EXCE=$(shell ls src/except/*.m)
CORE=$(shell ls src/core/*.m)

# list the source code for the lexer
SOURCES=$(MAIN) $(TOK) $(EXCE) $(CORE)

# compile the source code for the lexer
OBJECTS=$(SOURCES:.m=.o)

# define the name of the executable
EXECUTABLE=mllexer

# start of build rules

# rule for building the lexer without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# rule for compiling the lexer without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(OBJECTS) obj/
	mv -f $@ bin/
	find . -type f -name '*.d' -delete

# rule for compiling the lexer with debug symbols
debug: $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(OBJECTS) obj/
	mv -f $(EXECUTABLE) bin/ 
	find . -type f -name '*.d' -delete

# rule for compiling source code to object code
.m.o:
	$(CC) $(CFLAGS) $< -o $@

# rule for installing the compiled lexer
# REQUIRES ROOT
install:
	cp bin/$(EXECUTABLE) /usr/bin/

# rule for cleaning the workspace
clean:
	rm -rf bin
	rm -rf obj 

# end of Makefile
